# Advise GitLab that these environment vars should be loaded from the Variables config.
variables:
    PROD_PYPI_TOKEN: SECURE
    TEST_PYPI_TOKEN: SECURE

include:
  - template: Dependency-Scanning.gitlab-ci.yml


gemnasium-python-dependency_scanning:
    rules:
      - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

stages:
    - test
    - build
    - deploy

.test:
    stage: test
    before_script:
        - python3 -V
        - pip3 install virtualenv
        - virtualenv venv
        - source venv/bin/activate
        - pip3 install -r requirements.txt
        - pip3 install -r ci-requirements.txt
    cache:
        key: python-packages-cache
        paths:
            - .cache/pip
            - venv/
    script:
        - pytest tests

test 3:8:
    image: python:3.8
    extends: ".test"

test 3:9:
    image: python:3.9
    extends: ".test"

test 3:10:
    image: python:3.10
    extends: ".test"

test 3:11:
    image: python:3.11
    extends: ".test"

coverage:
    image: python:3.9
    extends: ".test"
    only:
        - master
        - merge_requests
    script:
        - pip3 install pytest-cov
        - pytest --cov=. tests
        - coverage xml
    coverage: '/TOTAL.+ ([0-9]{1,3}%)/'

doc:
    image: python:3.9
    stage: build
    only:
        - master
        - merge_requests
    before_script:
        - python3 -V
        - pip3 install virtualenv
        - virtualenv venv
        - source venv/bin/activate
        - pip3 install -r requirements.txt
        - pip3 install -r requirements.fastapi.txt
        - pip3 install -r requirements.extras.txt
    script:
        - pip3 install -r doc/requirements.txt
        - mkdocs build -f doc/mkdocs.yml
    artifacts:
        paths:
          - doc/site/
        expire_in: 1d

pages:
    stage: deploy
    only:
        - master
    dependencies:
        - doc
    script:
        - mkdir -p public/
        - cp -fr doc/site/* public/ || true
    artifacts:
        paths:
            - public

test_pypi:
    image: python:3.7
    stage: build
    variables:
        TWINE_USERNAME: "__token__"
        TWINE_PASSWORD: $TEST_PYPI_TOKEN
    script:
        - pip -q install --upgrade pip setuptools wheel pkginfo
        - pip install twine
        - python setup.py sdist bdist_wheel
        - twine upload --repository-url https://test.pypi.org/legacy/ dist/*
    only:
        - master
    except:
        - /^contrib-\d+\.\d+(\.\d+)?(\.?(a|b|rc)\d*)?$/   # version tag

upload_pypi:
    image: python:3.7
    stage: deploy
    when: manual
    variables:
        TWINE_USERNAME: "__token__"
        TWINE_PASSWORD: $PROD_PYPI_TOKEN
    script:
        - pip -q install --upgrade pip setuptools wheel pkginfo
        - pip -q install twine
        - python setup.py sdist bdist_wheel
        - twine upload dist/*
    only:
        - /^contrib-\d+\.\d+(\.\d+)?(\.?(a|b|rc)\d*)?$/   # version tag

tag_prepare:
    image: debian:stable
    stage: deploy
    rules:
        - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH && $CI_COMMIT_MESSAGE =~ /v([0-9\.]+)/
    before_script:
        - apt-get update
        - export DEBIAN_FRONTEND=noninteractive
        - apt-get -y install git curl jq gawk
    script:
        # Get version from MR commit (vX.Y.Z), exit early if tag already exists
        - export VERSION=$(echo "$CI_COMMIT_MESSAGE" | gawk 'match($0, /v([0-9\.]+)/, a) {print a[1]}')
        - test -z $VERSION && exit 0    # exit if no version specified
        - export VERSION=contrib-$VERSION
        - export VERSION_TAG_EXISTS=$(git tag | grep $VERSION | wc -l)
        - test $VERSION_TAG_EXISTS -eq 1 && exit 0  # exit if version already exist
        - echo VERSION=$VERSION >> variables.env
    artifacts:
        reports:
            dotenv: variables.env

tag_release:
    image: registry.gitlab.com/gitlab-org/release-cli:latest
    stage: deploy
    rules:
        - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH && $CI_COMMIT_MESSAGE =~ /v([0-9\.]+)/
    needs:
        - job: tag_prepare
          artifacts: true
    script:
        - echo "Creating release $VERSION"
    release:
        tag_name: $VERSION
        description: $VERSION
