# Patchwork Contrib

Set of tools and helpers to integrate [Patchwork](https://patchwork-framework.gitlab.io/core) with external
libraries.

## Warning!
Current project state should be considered as Proof-of-Concept. 

This framework is based on working solution which handles 100+ services and hundreds of events per second,
however it's written from scratch and fully asynchronous in opposite to original one which is fully synchronous. 
There is no guarantee that this framework pass performance tests and will be useful.


https://patchwork-framework.gitlab.io/contrib
