# Patchwork Contrib

### FastAPI: 
`patchwork-contrib[fastapi]`

To enable sessions install also [starsessions](https://pypi.org/project/starsessions/).

If you'd like to use User model install [Tortoise ORM](https://pypi.org/project/tortoise-orm/).

