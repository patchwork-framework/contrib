# Patchwork Kafka Client

Project source code can be found at [GitLab](https://gitlab.com/patchwork-framework/client-kafka).

Package name: `patchwork-contrib`

This package contains a client implementation for Kafka message broker.

## FastAPI

::: patchwork.contrib.fastapi

