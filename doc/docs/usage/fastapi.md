# Integration with FastAPI

To use this integration you must have [FastAPI](https://pypi.org/project/fastapi/) installed.
If `patchwork-contrib` was installed using `fastapi` extra (`patchwork-contrib[fastapi]`), this dependency
is already installed.

FastAPI is a fast, asynchronous HTTP server for Python. This integration tools provides some helpers to
interact with Patchwork ecosystem:
- integration with **publishers**, so FastAPI app can easily send tasks to Patchwork

and some extra common features:
- support for sessions
- support for JWT token
- users support

## Configuration

To configure the integration call `register_patchwork` method with the FastAPI application and 
settings model as arguments.

```python
from patchwork.contrib.fastapi import PatchworkFastAPISettings, register_patchwork
from fastapi import FastAPI

app = FastAPI()

# these settings can be loaded from .env file or envs, see Pydantic for more details
settings = PatchworkFastAPISettings()

register_patchwork(app, settings)
```

The `register_patchwork` method do all configuration needed to integrate Patchwork and FastAPI including
registering some `startup` and `shutdown` callbacks and initiating Dependencies.

### Available settings

::: patchwork.contrib.fastapi.settings

## Dependencies

#### get_publisher()
Returns an instance of configured publisher. It can be used to send a task.

!!! danger
    This dependency required publisher to be configured.

#### current_user_id()
Returns an ID of current user. The value is taken from JWT.

If there is no current user signed in, raises `HTTPException` with `401` error code.

!!! danger
    This dependency requires JWT to be configured.

#### optional_current_user_id()
Same as `current_user_id` but returns `None` if there is no current user instead of raising an exception.

#### current_user()
Returns an instance of current user. Instance is loaded from model given in settings, ID is obtained
using `current_user_id()`.

If there is no current user signed in or user of that ID does not exist in the users table raises `HTTPException`
with `401` error code.

!!! danger
    This dependency requires JWT to be configured. 

    This dependency requires `user_model` to be configured and [Tortoise ORM](https://pypi.org/project/tortoise-orm/) to be installed.

#### optional_current_user()
Same as `current_user()` but instead of raising an exception when there is no user signed in or user 
does not exist, returns `None`.

#### current_session()
Returns current session instance. Session data can be mutated, in such case sessions data are
automatically persisted.

!!! danger
    This dependency requires sessions to be configured.

    This dependency requires [starsessions](https://pypi.org/project/starsessions/) to be installed.

## Sessions

If [starsessions](https://pypi.org/project/starsessions/) is installed, sessions middleware is added
to FastAPI to provide sessions support.

Sessions must be configured before usage.
To get current session and mutate it's state use `current_session` dependency.


